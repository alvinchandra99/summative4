package com.nexsoft.summative4.Model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Department implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @OneToMany(mappedBy ="department", fetch = FetchType.LAZY)
    private List<User> user;

    public Department() {
    }


    public Department(int id, String name, List<User> user) {
        this.id = id;
        this.name = name;
        this.user = user;
    }

    

    public List<User> getUser() {
        return user;
    }



    public void setUser(List<User> user) {
        this.user = user;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
