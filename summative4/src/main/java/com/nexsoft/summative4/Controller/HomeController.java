package com.nexsoft.summative4.Controller;

import java.util.ArrayList;
import com.nexsoft.summative4.Model.User;
import com.nexsoft.summative4.Service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    UserService userService;

    @GetMapping
    public String home(Model model) {
    
        model.addAttribute("totalUser", userService.findAll().size());
        model.addAttribute("listUser", userService.findAll());

        return "index";
    }

    @PostMapping(value = "/save")
    public String save(@RequestBody User user) {

        userService.save(user);
        return "redirect:/";
    }

    @PostMapping(value="/delete/{id}")
    public String delete(@PathVariable int id){
        userService.delete(id);
        return "redirect:/";
    }
    
    @PostMapping("/saveAll")
    public String saveAll(@RequestBody ArrayList<User> listUser){
        ArrayList<User> listUserNotNull = new ArrayList<User>();

        for(User user : listUser){
            if(user.getName().isEmpty() || user.getEmail().isEmpty() || user.getLastName().isEmpty() || user.getBirthdate().isEmpty() || user.getDepartment().getName().isEmpty()){
              continue;  
            }
            listUserNotNull.add(user);
        }
        userService.saveAll(listUserNotNull);
        return "redirect:/";
    }

    @GetMapping(value="/edit/{id}")
    public String edit(@PathVariable int id, Model model){
        model.addAttribute("user", userService.findById(id));
        return "edit";
    }



}
