package com.nexsoft.summative4.Repository;

import java.util.List;

import com.nexsoft.summative4.Model.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    public List<User> findAll();

}
