package com.nexsoft.summative4.Service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.summative4.Model.User;
import com.nexsoft.summative4.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@TransactionScoped
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }
    
    public void delete(int id){
        userRepository.deleteById(id);
    }

    public void saveAll(ArrayList<User> listUser){
        userRepository.saveAll(listUser);
    }

    public User findById(int id){
        return userRepository.findById(id).get();
    }

}
