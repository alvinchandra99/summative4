<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglibprefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous"
    />

    <!-- Bootstrap Icons-->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container mt-5">
      <table class="table table-bordered">
        <thead class="table-light">
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Birthdate</th>
            <th scope="col">Deparment</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody id="tableUser"></tbody>
      </table>
      <button class="btn btn-primary" onclick="saveAll()">Save</button>
    </div>

    <script>
      let totalUser = parseInt("${totalUser}");

      let update = `
      <!-- Update -->
          <c:forEach items="${listUser}" var="u">
            <tr>
              <td>
                <input type="hidden" name="userId" value="${u.getId()}">
                <input
                  type="text"
                  name="name"
                  class="form-control"
                  value="${u.getName()}"
                />
              </td>
              <td>
                <input
                  type="text"
                  name="lastName"
                  class="form-control"
                  value="${u.getLastName()}"
                />
              </td>
              <td>
                <input
                  type="email"
                  name="email"
                  class="form-control"
                  value="${u.getEmail()}"
                />
              </td>
              <td>
                <input
                  type="date"
                  name="birthdate"
                  class="form-control"
                  value="${u.getBirthdate()}"
                />
              </td>
              <td>
                <input
                  type="text"
                  name="department"
                  class="form-control"
                  value="${u.getDepartment().getName()}"
                />
              </td>
              <td style="width: 150px">
                <form action="/delete/${u.getId()}" method="post">
                <button
                  type = "submit"
                  class="btn"
                  data-bs-toggle="modal"
                  data-bs-target="#deleteModal"
                >
                  <i class="bi bi-x-circle"></i>
                </button>
                </form>

                <form action="edit/${u.getId()}" method="get">
                <button
                  class="btn"
                >
                  <i class="bi bi-check-circle"></i>
                </button>
                </form>
              </td>
            </tr>
          </c:forEach>
          <!--Update End-->
      `;

      // let totalUpdate = "";
      // for(i = 0 ; i < totalUser; i ++){

      // }

      let totalCreate = "";
      for (i = 0; i < 10; i++) {
        let create =
          `  <!--Create -->
          <tr>
            <td><input type="text" name="name" class="form-control" /></td>
            <td><input type="text" name="lastName" class="form-control" /></td>
            <td><input type="email" name="email" class="form-control" /></td>
            <td><input type="date" name="birthdate" class="form-control" /></td>
            <td>
              <input type="text" name="department" class="form-control" />
            </td>
            <td style="width: 150px">


              <button class="btn" onclick="save(` +
          i +
          `)">
                <i class="bi bi-check-circle"></i>
              </button>
            </td>
          </tr>`;
        totalCreate = totalCreate + create;
      }
      document.getElementById("tableUser").innerHTML = update + totalCreate;

      function save(index) {
        let name = document.getElementsByName("name")[index + totalUser].value;

        let lastName =
          document.getElementsByName("lastName")[index + totalUser].value;
        let email =
          document.getElementsByName("email")[index + totalUser].value;
        let birthdate =
          document.getElementsByName("birthdate")[index + totalUser].value;
        let deparment =
          document.getElementsByName("department")[index + totalUser].value;

        let data = {
          name: name,
          lastName: lastName,
          email: email,
          birthdate: birthdate,
          department: { name: deparment },
        };

        let options = {
          method: "post",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
          body: JSON.stringify(data),
        };
        let alertMsg = confirm("Yakin Ingin Menyimpan Data");
        if (alertMsg == true) {
          let save = fetch("http://localhost:8080/save", options);
          window.open("http://localhost:8080");
        }
      }

      function saveAll() {
        let data = [];
        for (i = 0; i < totalUser + 10; i++) {
          let name = document.getElementsByName("name")[i].value;
          let lastName = document.getElementsByName("lastName")[i].value;
          let email = document.getElementsByName("email")[i].value;
          let birthdate = document.getElementsByName("birthdate")[i].value;
          let deparment = document.getElementsByName("department")[i].value;

          if (i < totalUser) {
            let id = document.getElementsByName("userId")[i].value;
            console.log(id);
            data.push({
              id: id,
              name: name,
              lastName: lastName,
              email: email,
              birthdate: birthdate,
              department: { name: deparment },
            });
          } else {
            data.push({
              name: name,
              lastName: lastName,
              email: email,
              birthdate: birthdate,
              department: { name: deparment },
            });
          }
        }
        console.log(data);
        let options = {
          method: "post",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
          body: JSON.stringify(data),
        };
        let save = fetch("http://localhost:8080/saveAll", options);
        // window.open("http://localhost:8080");
      }
    </script>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
