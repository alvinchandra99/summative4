<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglibprefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous"
    />

    <!-- Bootstrap Icons-->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container mt-5" style="width: 500px">
      <div class="row g-4">
        <div class="col-6">Name :</div>
        <div class="col-6">
          <input
            name="name"
            type="text"
            class="form-control"
            value="${user.getName()}"
          />
        </div>

        <div class="col-6">LastName :</div>
        <div class="col-6">
          <input
            name="lastName"
            type="text"
            class="form-control"
            value="${user.getLastName()}"
          />
        </div>

        <div class="col-6">Email :</div>
        <div class="col-6">
          <input
            name="email"
            type="email"
            class="form-control"
            value="${user.getEmail()}"
          />
        </div>

        <div class="col-6">Birthdate :</div>
        <div class="col-6">
          <input
            name="birthdate"
            type="date"
            class="form-control"
            value="${user.getBirthdate()}"
          />
        </div>

        <div class="col-6">Department :</div>
        <div class="col-6">
          <input
            name="department"
            type="text"
            class="form-control"
            value="${user.getDepartment().getName()}"
          />
        </div>
        <button class="btn btn-primary" onclick="update('${user.getId()}')">
          Update
        </button>
      </div>
    </div>
    <script>
      function update(id) {
        let name = document.getElementsByName("name")[0].value;
        let lastName = document.getElementsByName("lastName")[0].value;
        let email = document.getElementsByName("email")[0].value;
        let birthdate = document.getElementsByName("birthdate")[0].value;
        let deparment = document.getElementsByName("department")[0].value;

        let data = {
          id: id,
          name: name,
          lastName: lastName,
          email: email,
          birthdate: birthdate,
          department: { name: deparment },
        };

        let options = {
          method: "post",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
          body: JSON.stringify(data),
        };
        let save = fetch("http://localhost:8080/save", options);
      }
    </script>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
